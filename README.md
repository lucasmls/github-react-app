# github-search-react-app

> This application uses [github-api](https://developer.github.com/v3/) to get informations and datas from Github. It was created just for study purposes.
> To see the application running, go to: http://react-github-search.surge.sh/

## How to Run

1. Run git clone
  - ssh: git clone `git@github.com:lucasmls/github-react-app.git`
  - https: git clone `https://github.com/lucasmls/github-react-app.git`
2. Install the dependencies with `npm install`.
3. Run your application with `npm start`.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
