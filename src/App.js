import React, { Component } from 'react'
import axios from 'axios'
import Container from './components/Container'


const initialReposState = {
  repos: [],
  pagination: {
    activePage: 1,
    total: 1
  }
}

class App extends Component {
  constructor() {
    super()
    this.handleSearch = this.handleSearch.bind(this)
    this.getRepositories = this.getRepositories.bind(this)
    this.getURL = this.getURL.bind(this)
    this.updateRepositoriesState = this.updateRepositoriesState.bind(this)
    this.perPage = 3

    this.state = {
      userInfo: null,
      repositories: initialReposState,
      selectedSection: '',
      isFetching: false
    }
  }

  async handleSearch(e) {
    const username = e.target.value
    const ENTER = 13
    const URL = this.getURL(username)
    const keyCode = e.which || e.keyCode

    // console.dir(e.target) Mostra as propriedades do target
    if (keyCode === ENTER) {
      this.setState({ isFetching: true })
      const user = (await axios.get(URL)).data
      const { name, login, avatar_url, html_url, location, bio, public_repos, following, followers } = user
      this.setState({
        userInfo: { name, login, avatar_url, html_url, location, bio, public_repos, following, followers },
        selectedSection: 'Repositórios'
      })
      this.getRepositories('repos')()
      this.setState({ isFetching: false })
    }
  }

  getURL(username, type, page = 1) {
    const internalUser = username ? `/${username}` : ''
    const internalType = type ? `/${type}` : ''
    return `https://api.github.com/users${internalUser}${internalType}?per_page=${this.perPage}&page=${page}`
  }

  updateRepositoriesState(type, repositories, page, totalPages) {
    this.setState({
      repositories: {
        repos: repositories,
        pagination: {
          activePage: page,
          total: totalPages
        }
      },
      selectedSection: type === 'starred' ? 'Favoritos' : 'Repositórios'
    })
  }

  getRepositories(type, page = 1) {
    return async () => {
      const URL = this.getURL(this.state.userInfo.login, type, page)
      const response = await axios.get(URL)
      const headersLink = response.headers.link || ''
      const totalPagesMatch = headersLink.match(/&page=(\d+)>; rel="last"/)
      const totalPages = totalPagesMatch ? +totalPagesMatch[1] : this.state.repositories.pagination.total

      const repositories = response.data.map(repository => ({
        id: repository.id,
        name: repository.name,
        html_url: repository.html_url
      }))

      this.updateRepositoriesState(type, repositories, page, totalPages)
    }
  }

  render() {
    return (
      <Container { ...this.state }
        handleSearch={this.handleSearch}
        getRepositories={this.getRepositories('repos')}
        getStarred={this.getRepositories('starred')}
        handlePagination={(type, page) => this.getRepositories(type, page)} />
    )
  }
}

export default App
