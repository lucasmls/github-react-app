import React from 'react'
import PropTypes from 'prop-types'

import './UserInfo.css'

const UserInfo = ({ userInfo }) => {
  return (
    <div className='user-info'>
      <div className='user-info__header'>
        <div className='container'>
          <a href={userInfo.html_url} target='blank'>
            <img className='user-info__img' src={userInfo.avatar_url} alt='' />
          </a>
          <h2 className='user-info__name'>
            <a
              className='user-info__name--link'
              href={userInfo.html_url}
              target='blank'
            >
              {userInfo.name}
            </a>
          </h2>
          <p className='user-info__bio'>{userInfo.bio}</p>
        </div>
        <h4 className='user-info__city'>{userInfo.location}</h4>
      </div>

      <div className='user-info__repos-info'>
        <div className='container'>
          <ul className='user-info__repos-info__list'>
            <li className='user-info__repos-info__list__item'>
              Repositórios: {userInfo.public_repos}
            </li>
            <li className='user-info__repos-info__list__item'>
              Seguidores: {userInfo.followers}
            </li>
            <li className='user-info__repos-info__list__item'>
              Seguindo: {userInfo.following}
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}

UserInfo.propTypes = {
  userInfo: PropTypes.shape({
    name: PropTypes.string.isRequired,
    login: PropTypes.string.isRequired,
    avatar_url: PropTypes.string.isRequired,
    html_url: PropTypes.string.isRequired,
    location: PropTypes.string,
    bio: PropTypes.string,
    public_repos: PropTypes.number.isRequired,
    following: PropTypes.number.isRequired,
    followers: PropTypes.number.isRequired
  })
}

export default UserInfo
