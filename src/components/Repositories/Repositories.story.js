import React from 'react'
import Repositories from './index'

import { storiesOf } from '@storybook/react'

const repositories = [
  { id: 1, name: 'Repositório 01', html_url: 'https://google.com.br' },
  { id: 2, name: 'Repositório 02', html_url: 'https://google.com.br' }
]

storiesOf('Repositories', module).add('Com a prop "title"', () => (
  <Repositories title='Repositórios' />
))

storiesOf('Repositories', module).add('Com a prop "repositories"', () => (
  <Repositories title='Repositórios' repositories={repositories} />
))
