import React from 'react'
import PropTypes from 'prop-types'
import Pagination from '../Pagination'

import './Repositories.css'

const Repositories = ({ title, repositories, handlePagination, selectedSection }) => {
  return (
    <div className='repositories'>
      <h2 className='repositories__section__title'>{title}</h2>

      <div className='repositories__section__stars'>
        <div className='container'>
          <ul className='repositories-list'>
            {repositories.repos.map(repo => (
              <li key={repo.id} className='repositories-list__item'>
                <a
                  className='repositories-list__item--link'
                  href={repo.html_url}
                  target='blank' >
                    {repo.name}
                </a>
              </li>
            ))}
          </ul>

          <Pagination
            total={repositories.pagination.total}
            activePage={repositories.pagination.activePage}
            onClick={handlePagination}
            selectedSection={selectedSection} />
        </div>
      </div>
    </div>
  )
}

Repositories.defaultProps = {
  title: '',
  repositories: { }
}

Repositories.propTypes = {
  title: PropTypes.string.isRequired,
  repositories: PropTypes.shape({
    repos: PropTypes.arrayOf(PropTypes.shape({
      html_url: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired
    })).isRequired,
    pagination: PropTypes.shape({
      activePage: PropTypes.number.isRequired,
      total: PropTypes.number.isRequired
    }).isRequired
  }).isRequired,
  handlePagination: PropTypes.func.isRequired,
  selectedSection: PropTypes.string.isRequired
}

export default Repositories
