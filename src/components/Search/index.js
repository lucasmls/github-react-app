import React from 'react'
import PropTypes from 'prop-types'

import './Search.css'

const Search = ({ handleSearch, isDisabled }) => {
  const handleSearchClick = e => {
    e.preventDefault()
    this.myInput.focus()
  }

  return (
    <div className='search-box'>
      <input
        className='search-box__input'
        type='text'
        placeholder='Nome do Usuário'
        ref={node => (this.myInput = node)}
        disabled={isDisabled}
        onKeyUp={handleSearch}
      />

      <a className='search-box__btn' href='' onClick={handleSearchClick}>
        <i className='fab fa-searchengin' />
      </a>
    </div>
  )
}

Search.propTypes = {
  handleSearch: PropTypes.func.isRequired,
  isDisabled: PropTypes.bool.isRequired
}

export default Search
