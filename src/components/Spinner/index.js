import React from 'react'
import spinner from './spin.svg'

import './Spinner.css'

const Spinner = () => (
  <div  className="spin-container" style={{ textAlign: 'center' }}>
    <img src={spinner} style={{ width: '50px', height: '50px' }} alt="Imagem do loader de carregamento" />
  </div>
)

export default Spinner