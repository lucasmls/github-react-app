import React from 'react'
import PropTypes from 'prop-types'

import './Actions.css'

const Actions = ({ getRepositories, getStarred, selectedSection }) => {
  return (
    <div className='actions'>
      <button
        className={`actions__btn ${selectedSection === 'Repositórios' ? 'actions__btn--active' : ''}`}
        onClick={getRepositories} >
        Repositórios
      </button>

      <button
        className={`actions__btn ${selectedSection === 'Favoritos' ? 'actions__btn--active' : ''}`}
        onClick={getStarred} >
        Favoritos
      </button>
    </div>
  )
}

Actions.propTypes = {
  getRepositories: PropTypes.func.isRequired,
  getStarred: PropTypes.func.isRequired,
  selectedSection: PropTypes.string.isRequired
}

export default Actions
