import React from 'react'
import Actions from './index'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

storiesOf('Actions', module).add('"Favoritos" selecionado', () => (
  <Actions
    selectedSection='Favoritos'
    getStarred={action('Get Starred')}
    getRepositories={action('Get Repositories')}
  />
))

storiesOf('Actions', module).add('"Repositórios" selecionado', () => (
  <Actions
    selectedSection='Repositórios'
    getStarred={action('Get Starred')}
    getRepositories={action('Get Repositories')}
  />
))
