import React from 'react'
import PropTypes from 'prop-types'

import Search from '../Search'
import UserInfo from '../UserInfo'
import Actions from '../Actions'
import Repositories from '../Repositories'
import Spinner from '../Spinner'

const Container = ({
  userInfo,
  repositories,
  selectedSection,
  handleSearch,
  getRepositories,
  getStarred,
  handlePagination,
  isFetching
}) => {
  const formattedSelectedSection = selectedSection === 'Favoritos' ? 'starred' : 'repos'
  return (
    <div className='app'>
      <Search handleSearch={handleSearch} isDisabled={isFetching} />

      {isFetching && <Spinner />}

      {!!userInfo && <UserInfo userInfo={userInfo} isFetching={isFetching} />}

      {!!userInfo &&
        <Actions
          getRepositories={getRepositories}
          getStarred={getStarred}
          selectedSection={selectedSection} />}

      {!!repositories.repos.length &&
        <Repositories
          selectedSection={selectedSection}
          handlePagination={page => handlePagination(formattedSelectedSection, page)}
          repositories={repositories}
          title={selectedSection} />}
    </div>
  )
}

Container.propTypes = {
  userInfo: PropTypes.shape({
    name: PropTypes.string.isRequired,
    login: PropTypes.string.isRequired,
    avatar_url: PropTypes.string.isRequired,
    html_url: PropTypes.string.isRequired,
    location: PropTypes.string.isRequired,
    bio: PropTypes.string,
    public_repos: PropTypes.number.isRequired,
    following: PropTypes.number.isRequired,
    followers: PropTypes.number.isRequired,
  }),
  repositories: PropTypes.shape({
    repos: PropTypes.arrayOf(PropTypes.shape({
      html_url: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired
    })).isRequired,
    pagination: PropTypes.shape({
      activePage: PropTypes.number.isRequired,
      total: PropTypes.number.isRequired
    }).isRequired
  }).isRequired,
  selectedSection: PropTypes.string.isRequired,
  handleSearch: PropTypes.func.isRequired,
  getRepositories: PropTypes.func.isRequired,
  getStarred: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired
}

export default Container
