import React from 'react'
import PropTypes from 'prop-types'
import Page from './Page'

import pagination from '../../utils/pagination'
import './styles.css'

const Pagination = ({ total, activePage, pageLink, onClick }) => (
  <ul className="pagination">
    {pagination({ total, activePage }).map((page, index) => (
      <li key={index} className={`pagination-item ${activePage === page ? 'active' : ''}`}>
        <Page
          isActive={activePage === page ? true : false}
          page={page}
          pageLink={pageLink.replace('%page%', page)}
          onClick={onClick} />
      </li>
    ))}
  </ul>
)

Pagination.defaultProps = {
  pageLink: ''
}

Pagination.propTypes = {
  total: PropTypes.number,
  activePage: PropTypes.number,
  pageLink: PropTypes.string,
  onClick: PropTypes.func
}

export default Pagination
