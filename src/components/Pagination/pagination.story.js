import React from 'react'
import Pagination from '../Pagination'

import { storiesOf } from '@storybook/react'

storiesOf('Pagination', module)
  .add('"Pagination" sem as props', () => (
    <Pagination />
  ))

storiesOf('Pagination', module)
  .add('"Pagination" com o total de páginas', () => (
    <Pagination total={10} />
  ))

storiesOf('Pagination', module)
  .add('"Pagination" com o total de páginas e a página ativa', () => (
    <Pagination total={10} activePage={6} />
  ))

storiesOf('Pagination', module)
  .add('"Pagination" com o link da página', () => (
    <Pagination total={15} activePage={5} pageLink={'https://google.com/page/%page%'}/>
  ))

storiesOf('Pagination', module)
  .add('"Pagination" com o callback do clique', () => (
    <Pagination
      total={10}
      activePage={6}
      pageLink={'https://google.com/page/%page%'}
      onClick={page => alert(page)}
    />
  ))

