import { expect } from 'chai'
import pagination from './index'

// Demonstração "visual" da paginação
// qtd || página ativa  || demonstração
// 5   ||       1       || [01, 2, 3, 4, 5]
// 5   ||       2       || [1, 02, 3, 4, 5]
// 5   ||       3       || [1, 2, 03, 4, 5]
// 5   ||       4       || [1, 2, 3, 04, 5]
// 5   ||       5       || [1, 2, 3, 4, 05]

// qtd || página ativa  || demonstração
// 6   ||       1       || [01, 2, 3, ... , 6]
// 6   ||       2       || [1, 02, 3, ... , 6]
// 6   ||       3       || [1, 2, 03, 4, 5, 6]

describe('Pagination', () => {
  it('Pagination should be a function', () => {
    expect(pagination).to.be.a('function')
  })

  it('pagination({ total: 1, activePage: 1 }) should return [1]', () => {
    const params = { total: 1, activePage: 1 }
    const result = [1]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 2, activePage: 1 }) should return [1, 2]', () => {
    const params = { total: 2, activePage: 1 }
    const result = [1, 2]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 5, activePage: 1 }) should return [1, 2, 3, 4, 5]', () => {
    const params = { total: 5, activePage: 1 }
    const result = [1, 2, 3, 4, 5]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 6, activePage: 1 }) should return [1, 2, 3, "...", 6]', () => {
    const params = { total: 6, activePage: 1 }
    const result = [1, 2, 3, '...', 6]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 6, activePage: 2 }) should return [1, 2, 3, "...", 6]', () => {
    const params = { total: 6, activePage: 2 }
    const result = [1, 2, 3, '...', 6]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 6, activePage: 3 }) should return [1, 2, 3, 4, 5, 6]', () => {
    const params = { total: 6, activePage: 3 }
    const result = [1, 2, 3, 4, 5, 6]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 6, activePage: 4 }) should return [1, 2, 3, 4, 5, 6]', () => {
    const params = { total: 6, activePage: 4 }
    const result = [1, 2, 3, 4, 5, 6]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 6, activePage: 5 }) should return [1, "..." 4, 5, 6]', () => {
    const params = { total: 6, activePage: 5 }
    const result = [1, '...', 4, 5, 6]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 6, activePage: 6 }) should return [1, "..." 4, 5, 6]', () => {
    const params = { total: 6, activePage: 6 }
    const result = [1, '...', 4, 5, 6]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 7, activePage: 1 }) should return [1, 2, 3, "...", 7]', () => {
    const params = { total: 7, activePage: 1 }
    const result = [1, 2, 3, '...', 7]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 7, activePage: 3 }) should return [1, 2, 3, 4, "...", 7]', () => {
    const params = { total: 7, activePage: 3 }
    const result = [1, 2, 3, 4, '...', 7]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 7, activePage: 4 }) should return [1, 2, 3, 4, 5, 6, 7]', () => {
    const params = { total: 7, activePage: 4 }
    const result = [1, 2, 3, 4, 5, 6, 7]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 7, activePage: 5 }) should return [1, "...", 4, 5, 6, 7]', () => {
    const params = { total: 7, activePage: 5 }
    const result = [1, '...', 4, 5, 6, 7]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 7, activePage: 6 }) should return [1, "...", 5, 6, 7]', () => {
    const params = { total: 7, activePage: 6 }
    const result = [1, '...', 5, 6, 7]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 7, activePage: 7 }) should return [1, "...", 5, 6, 7]', () => {
    const params = { total: 7, activePage: 7 }
    const result = [1, '...', 5, 6, 7]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination({ total: 15, activePage: 8 }) should return [1, "...", 7, 8, 9, "...", 15]', () => {
    const params = { total: 15, activePage: 8 }
    const result = [1, '...', 7, 8, 9, '...', 15]
    expect(pagination(params)).to.be.deep.equal(result)
  })

  it('pagination() should return [1]', () => {
    const result = [1]
    expect(pagination()).to.be.deep.equal(result)
  })

  it('pagination({ total: "abc", activePage: 1 }) should return "total" must be a Number', () => {
    const params = { total: "abc", activePage: 1 }
    const result = '"total" must be a Number'
    try {
      pagination(params)
    } catch (error) {
      expect(error.message).to.be.equal(result)
    }
  })

  it('pagination({ total: 10, activePage: "1a" }) should return "activePage" must be a Number', () => {
    const params = { total: 10, activePage: "1a" }
    const result = '"activePage" must be a Number'
    try {
      pagination(params)
    } catch (error) {
      expect(error.message).to.be.equal(result)
    }
  })
})
