const centerRule = ({ total, activePage }) => {
  if (activePage - 1 <= 0) return 1
  if (activePage === total) return activePage - 2
  return activePage - 1
}

const removeDuplicates = pages =>
  pages.filter((page, index, pagesArray) => pagesArray.indexOf(page) === index)

const verifyParams = ({ total, activePage }) => {
  if (typeof total !== 'number')
    throw new TypeError('"total" must be a Number')

  if (typeof activePage !== 'number')
    throw new TypeError('"activePage" must be a Number')
}

const pagination = ({ total = 1, activePage = 1 } = {}) => {

  verifyParams({ total, activePage })

  // Array.apply(null, { length: total }).map((_, i) => i +1) // ES5
  if (total <= 5) return Array.from({ length: total }, (_, i) => i + 1) // ES6

  const VISIBLE_PAGES = 3
  let pages = [
    1,
    ...Array.from(
      { length: VISIBLE_PAGES },
      (_, i) => i + centerRule({ total, activePage })
    ),
    total
  ]

  pages = removeDuplicates(pages)

  let FIRST_PAGE = pages[0]
  let SECOND_PAGE = pages[1]

  if (SECOND_PAGE === FIRST_PAGE + 2) {
    pages = [FIRST_PAGE, FIRST_PAGE + 1, ...pages.slice(1)]
  }

  FIRST_PAGE = pages[0]
  SECOND_PAGE = pages[1]

  if (SECOND_PAGE > FIRST_PAGE + 2) {
    pages = [FIRST_PAGE, '...', ...pages.slice(1)]
  }

  let LAST_PAGE = pages[pages.length - 1]
  let PENULTIMATE_PAGE = pages[pages.length - 2]

  if (PENULTIMATE_PAGE === LAST_PAGE - 2) {
    pages = [
      ...pages.slice(0, -1), // pages: Array<number> sem a última página
      LAST_PAGE - 1, // Adicionando a penultima página, já que só existe uma página entre a selecinada e a última página
      LAST_PAGE // Última página
    ]
  }

  LAST_PAGE = pages[pages.length - 1]
  PENULTIMATE_PAGE = pages[pages.length - 2]

  if (PENULTIMATE_PAGE <= LAST_PAGE - 2) {
    pages = [...pages.slice(0, -1), '...', LAST_PAGE]
  }

  return pages
}

export default pagination
